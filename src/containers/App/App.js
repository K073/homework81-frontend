import React, {Component} from 'react';
import './App.css';
import {connect} from "react-redux";
import {changeLink, sendLink} from "../../store/actions/actions";

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">Welcome to LinkShorter</h1>
                </header>
                <p className="App-intro">
                    <input
                        type="text"
                        placeholder={"Enter URL here"}
                        onChange={(event) => {this.props.changeLink(event.target.value)}}
                        name={'link'}
                        value={this.props.link}
                    />
                    <button onClick={() => this.props.sendLink(this.props.link)}>Shorten!</button>
                </p>
                {this.props.shortenLink ? <a href={this.props.shortenLink}>{this.props.shortenLink}</a> : null}
            </div>
        );
    }
}

const initMapStateToProps = (state) => {
    return {
        shortenLink: state.shortenLink,
        link: state.link
    }
};

const initMapDispatchToProps = (dispatch) => {
    return {
        sendLink: (link) => dispatch(sendLink(link)),
        changeLink: (value) => dispatch(changeLink(value))
    }
};

export default connect(initMapStateToProps, initMapDispatchToProps)(App);
