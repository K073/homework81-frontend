import axios from '../../axios';

export const SEND_LINK_REQUEST = 'SEND_LINK_REQUEST';
export const SEND_LINK_SUCCESS = 'SEND_LINK_SUCCESS';
export const SEND_LINK_ERROR = 'SEND_LINK_ERROR';

export const CHANGE_LINK = 'CHANGE_LINK';

export const changeLink = (value) => {
    return { type: CHANGE_LINK, value}
};

export const sendLinkRequest = () => {
    return { type: SEND_LINK_REQUEST };
};

export const sendLinkSuccess = (data) => {
    return { type: SEND_LINK_SUCCESS, data};
};

export const sendLinkError = () => {
    return { type: SEND_LINK_ERROR };
};

export const sendLink = (value) => {
    return dispatch => {
        dispatch(sendLinkRequest());
        axios.post('/links', {url: value}).then(response => {
            dispatch(sendLinkSuccess(response.data));
        }, error => {
            dispatch(sendLinkError());
        });
    }
};
