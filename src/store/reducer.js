import {CHANGE_LINK, SEND_LINK_SUCCESS} from "./actions/actions";

const initialState = {
    link: '',
    shortenLink: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_LINK:
            return {...state, link: action.value};
        case SEND_LINK_SUCCESS:
            return {...state, shortenLink: action.data.shortenLink};
        default:
            return state;
    }
};

export default reducer;
