import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import reducer from "./store/reducer";
import { Provider } from 'react-redux';

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

const application = <Provider store={store}><App /></Provider>;

ReactDOM.render(application , document.getElementById('root'));
registerServiceWorker();